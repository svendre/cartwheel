extends Node2D

var mouseOrigin
var windowSiz
var windowCenter: Vector2
var slices
var angles = 0.0
var mousePos
var windowPos
var windowNewPos
var radius
var selected = false
var test = true
var moved = false
var screenSize = DisplayServer.screen_get_usable_rect()
@onready var icon = preload("res://folder-trash.svg")
var iconSize = 64
var iconHalf = iconSize/2
var apps = ["konsole","kolourpaint"]
var files = []



func _ready():
	windowSiz = get_viewport().size
	windowCenter = windowSiz/2
	#mousePos = DisplayServer.mouse_get_position()
	#windowNewPos = mousePos-windowSiz/2
	mouseOrigin = DisplayServer.mouse_get_position();
	DisplayServer.warp_mouse(windowCenter)
	#get desktop files
	get_desktopFiles()
	for i in files:
		pass
	slices = float(files.size())
	angles = 360/slices
	
func get_desktopFiles():
	var home_dir = OS.get_environment("HOME")
	var path = home_dir+"/NewFolder/"
	var dir = DirAccess.open(path)
	if dir:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				print("Found directory: " + file_name)
			else:
				print("Found file: " + file_name)
				var file = FileAccess.open(path+file_name,FileAccess.READ)
				if file:
					while not file.eof_reached():
						var line = file.get_line().strip_edges()
						if line.begins_with("Exec="):
							files.append(line.substr(5))
							print(line.substr(5))
					file.close()
			file_name = dir.get_next()
	

func _input(event): 
	if event.is_action("RMB"):
		#DisplayServer.warp_mouse(mouseOrigin)
		get_tree().quit()
	if event.is_action_pressed("LMB"):
		#print(Vector2(windowCenter-get_viewport().get_mouse_position()))
		select_slice()

		
var listReady = false
func _process(delta):
	if !listReady:
		for i in slices:
			pass
		listReady=true
	###TODO: APPEAR AT MOUSE
#	if test:
#		check if new window possition
#		if !moved && DisplayServer.window_get_position() != windowNewPos:
#			print("old: "+str(DisplayServer.window_get_position())+"    new: "+str(mousePos-windowSiz/2))
#			DisplayServer.window_set_position(windowNewPos)
#			moved = true
#		if moved && DisplayServer.window_get_position() == windowNewPos:
#			test = false
#		elif windowNewPos[0]<screenSize.position[0] || windowNewPos[0]>screenSize.size[0] || windowNewPos[1]<screenSize.position[1] || windowNewPos[1]>screenSize.size[1]:
#			await get_tree().create_timer(0.01).timeout
#			test = false
#		else: 
#			print("old: "+str(DisplayServer.window_get_position())+"    new: "+str(windowNewPos)+"    moved: "+str(moved))
#		windowPos=DisplayServer.window_get_position()
#		DisplayServer.warp_mouse(Vector2i(windowCenter,windowCenter))
	if windowCenter.distance_to(get_global_mouse_position())>radius:
		select_slice()

func select_slice():
		if !selected:
			selected=true
		var vec = Vector2(get_viewport().get_mouse_position()-windowCenter)
		var degs = rad_to_deg((vec).angle())+90
		if degs < 0: degs += 360
		var app = int(degs/360*slices)
		Thread.new().start(run_shell_script.bind(app))
		get_tree().quit()
		
func run_shell_script(app):
		OS.execute("sh",["-c",files[app]])
		
		
func draw_circle_arc_poly(center, radius, angle_from, angle_to, color):
	var nb_points = 32
	var points_arc = PackedVector2Array()
	points_arc.push_back(center)
	var colors = PackedColorArray([color])

	for i in range(nb_points + 1):
		var angle_point = deg_to_rad(angle_from + i * (angle_to - angle_from) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)
		
	draw_polygon(points_arc, colors)
	
func _draw():
	var center: Vector2 = windowCenter
	radius = windowSiz[0]/2
	var angle_from = 0
	var angle_to = 0
	var color = Color(1/slices, 0.0, 0.0)
	var offset
	var tauSlices = TAU/slices
	if int(slices) % 2 == 0:
		offset = tauSlices/2+PI/2
	else:
		offset = PI/2
	for i in slices:
		angle_to = angle_from + angles
		draw_circle_arc_poly(center, radius, angle_from, angle_to, color)
		angle_from = angle_to
		color[0] += 1/slices
	for i in slices:
		draw_texture(icon,windowCenter-Vector2(iconHalf,iconHalf)+Vector2((radius-iconSize)*cos((tauSlices*i)+offset),(radius-iconSize)*sin((tauSlices*i)+offset)))
		#(radius*cos(TAU/slices*i)),(radius*sin(TAU/slices*i)))
	#green slice:
	#draw_circle_arc_poly(center, radius/2, 0, angles, Color(0.0, 1.0, 0.0))
